package com.evilcorp.soulmanager

import com.evilcorp.soulmanager.repository.UserRepository
import com.evilcorp.soulmanager.repository.ValueAbleGoodRepository
import com.evilcorp.soulmanager.repository.findOne
import com.evilcorp.soulmanager.resolver.LoginResolver
import com.evilcorp.soulmanager.resolver.TransactionResolver
import com.evilcorp.soulmanager.resolver.UserResolver
import org.junit.Assert.*
import org.junit.FixMethodOrder
import org.junit.Test
import org.junit.runner.RunWith
import org.junit.runners.MethodSorters
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.test.context.ActiveProfiles
import org.springframework.test.context.junit4.SpringRunner


@RunWith(SpringRunner::class)
@SpringBootTest
@FixMethodOrder(MethodSorters.JVM)
@ActiveProfiles("testdata")
class TransactionTests : BaseTest() {

    @Autowired
    lateinit var valueAbleGoodRepository: ValueAbleGoodRepository
    @Autowired
    lateinit var transactionResolver: TransactionResolver

    @Test
    fun depositOwnAccount() {
        val user = userRepository.findOne(1) !!
        useSecurityUser(user) {
            val account = valueAbleGoodRepository.findOne(1) !!
            assertTrue(transactionResolver.deposit(account.id, 10))
            val accountAfter = valueAbleGoodRepository.findOne(1) !!
            assertEquals(account.value + 10, accountAfter.value)
        }
    }

    @Test
    fun depositOtherAccount() {
        val user = userRepository.findOne(2) !!
        useSecurityUser(user) {
            val account = valueAbleGoodRepository.findOne(1) !!
            assertFalse(transactionResolver.deposit(account.id, account.value))
            val accountAfter = valueAbleGoodRepository.findOne(1) !!
            assertEquals(account.value, accountAfter.value)
        }
    }

    @Test
    fun withdrawOwnAccount() {
        val user = userRepository.findOne(1) !!
        useSecurityUser(user) {
            val account = valueAbleGoodRepository.findOne(1) !!
            assertTrue(transactionResolver.withdraw(account.id, 5))
            val accountAfter = valueAbleGoodRepository.findOne(1) !!
            assertEquals(account.value - 5, accountAfter.value)
        }
    }

    @Test
    fun withdrawMoreThanAvailableMoneyFromAccount() {
        val user = userRepository.findOne(1) !!
        useSecurityUser(user) {
            val account = valueAbleGoodRepository.findOne(1) !!
            assertFalse(transactionResolver.withdraw(account.id, 100000))
            val accountAfter = valueAbleGoodRepository.findOne(1) !!
            assertEquals(account.value, accountAfter.value)
        }
    }

    @Test
    fun withdrawOtherAccount() {
        val user = userRepository.findOne(2) !!
        useSecurityUser(user) {
            val account = valueAbleGoodRepository.findOne(1) !!
            assertFalse(transactionResolver.withdraw(account.id, account.value))
            val accountAfter = valueAbleGoodRepository.findOne(1) !!
            assertEquals(account.value, accountAfter.value)
        }
    }

    @Test
    fun sendFromOwnAccount() {
        val user = userRepository.findOne(1) !!
        useSecurityUser(user) {
            val account1 = valueAbleGoodRepository.findOne(1) !!
            val account2 = valueAbleGoodRepository.findOne(2) !!
            assertTrue(transactionResolver.send(account1.id, account2.id, 2))
            val account1After = valueAbleGoodRepository.findOne(1) !!
            val account2After = valueAbleGoodRepository.findOne(2) !!
            assertEquals(account1.value - 2, account1After.value)
            assertEquals(account2.value + 2, account2After.value)
        }
    }

    @Test
    fun sendToSameAccountFail() {
        val user = userRepository.findOne(1) !!
        useSecurityUser(user) {
            val account = valueAbleGoodRepository.findOne(1) !!
            assertFalse(transactionResolver.send(account.id, account.id, 2))
            val accountAfter = valueAbleGoodRepository.findOne(1) !!
            assertEquals(account.value, accountAfter.value)
        }
    }

    @Test
    fun sendFromOtherAccount() {
        val user = userRepository.findOne(2)!!
        useSecurityUser(user) {
            val account1 = valueAbleGoodRepository.findOne(1) !!
            val account2 = valueAbleGoodRepository.findOne(2) !!
            assertFalse(transactionResolver.send(account1.id, account2.id, 2))
            val account1After = valueAbleGoodRepository.findOne(1) !!
            val account2After = valueAbleGoodRepository.findOne(2) !!
            assertEquals(account1.value, account1After.value)
            assertEquals(account2.value, account2After.value)
        }
    }
}