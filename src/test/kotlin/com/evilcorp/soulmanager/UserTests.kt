package com.evilcorp.soulmanager

import com.evilcorp.soulmanager.entity.User
import com.evilcorp.soulmanager.repository.UserRepository
import com.evilcorp.soulmanager.repository.ValueAbleGoodRepository
import com.evilcorp.soulmanager.repository.findOne
import com.evilcorp.soulmanager.resolver.LoginResolver
import com.evilcorp.soulmanager.resolver.TransactionResolver
import com.evilcorp.soulmanager.resolver.UserResolver
import org.junit.Assert.*
import org.junit.FixMethodOrder
import org.junit.Test
import org.junit.runner.RunWith
import org.junit.runners.MethodSorters
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.security.core.userdetails.UsernameNotFoundException
import org.springframework.test.context.ActiveProfiles
import org.springframework.test.context.junit4.SpringRunner
import javax.annotation.PostConstruct
import javax.transaction.Transactional


@RunWith(SpringRunner::class)
@SpringBootTest
@FixMethodOrder(MethodSorters.JVM)
@ActiveProfiles("testdata")
class UserTests : BaseTest() {


    @Autowired
    lateinit var loginResolver: LoginResolver
    @Autowired
    lateinit var userResolver: UserResolver
    @Autowired
    lateinit var valueAbleGoodRepository: ValueAbleGoodRepository
    lateinit var user: User

    @PostConstruct
    fun initUser() {
        user = userRepository.findOne(1)!!
    }

    @Test
    fun signup() {
        val userCount = userRepository.count()
        val user = user.copy(id = 0, email = "goblin3@evilcorp.com", username = "Vixekz")
        val result = loginResolver.signup(user)
        val userCountAfter = userRepository.count()
        assertEquals(userCount + 1, userCountAfter)
        assertEquals(userCount + 1, result.user.id)
        assertEquals(user.email, result.user.email)
        assertNotNull(result.token)
    }

    @Test(expected = Exception::class)
    fun signupSameEmailFail() {
        loginResolver.signup(user.copy(username = "aaa"))
    }

    @Test(expected = Exception::class)
    fun signupSameNameFail() {
        loginResolver.signup(user.copy(email = "aaa@aa.aa"))
    }

    @Test
    fun signin() {
        val result = loginResolver.signin(user.email, user.password)
        result?.let {
            assertEquals(user.id, result.user.id)
            assertEquals(user.email, result.user.email)
            assertNotNull(result.token)
        }
    }

    @Test(expected = UsernameNotFoundException::class)
    fun signinUsernameFail() {
        loginResolver.signin("123@fakemail.com", "")
    }

    @Test
    fun signinPasswordFail() {
        assertNull(loginResolver.signin(user.email, ""))
    }

    @Test
    @Transactional
    fun listUsers() {
        useRoleUser {
            val usersDB = userRepository.findAll()
            val users = userResolver.listUsers()
            assertEquals(usersDB, users)
        }
    }

    @Test(expected = Exception::class)
    fun listUsersNotLoggedInFail() {
        useRoleNone {
            userResolver.listUsers()
        }
    }

    @Test
    @Transactional
    fun listUser() {
        useRoleAdmin {
            val userDB = userRepository.findByEmail(user.email)!!
            val user = userResolver.listUser(userDB.id)
            assertEquals(userDB, user)
        }
    }

    @Test(expected = Exception::class)
    fun listUserNonAdmin() {
        useRoleUser {
            val userDB = userRepository.findByEmail(user.email)!!
            userResolver.listUser(userDB.id)
        }
    }

    @Test
    @Transactional
    fun listMyAccounts() {
        val user = userRepository.findOne(1)!!
        useSecurityUser(user) {
            val accounts = userResolver.listMyAccounts()
            val accountsDB = valueAbleGoodRepository.findByOwnerUsername(user.username)
            assertEquals(accountsDB, accounts)
        }
    }

    @Test
    @Transactional
    fun listAccountOwner() {
        val user = userRepository.findOne(1)!!
        useSecurityUser(user) {
            val account = userResolver.listAccount(1)
            val accountDB = valueAbleGoodRepository.findOne(1)!!
            assertEquals(accountDB, account)
        }
    }

    @Test(expected = Exception::class)
    fun listAccountNotOwner() {
        val user = userRepository.findOne(2)!!
        useSecurityUser(user) {
            userResolver.listAccount(1)
        }
    }

    @Test
    @Transactional
    fun listAccountAdmin() {
        val user = userRepository.findByUsername("bankelini")!!
        useSecurityUser(user) {
            val account = userResolver.listAccount(1)
            val accountDB = valueAbleGoodRepository.findOne(1)!!
            assertEquals(accountDB, account)
        }
    }


}