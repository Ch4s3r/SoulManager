package com.evilcorp.soulmanager

import com.evilcorp.soulmanager.entity.User
import com.evilcorp.soulmanager.repository.UserRepository
import com.evilcorp.soulmanager.repository.ValueAbleGoodRepository
import com.evilcorp.soulmanager.repository.findOne
import com.evilcorp.soulmanager.resolver.LoginResolver
import com.evilcorp.soulmanager.resolver.TransactionResolver
import com.evilcorp.soulmanager.resolver.UserResolver
import org.junit.FixMethodOrder
import org.junit.runner.RunWith
import org.junit.runners.MethodSorters
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken
import org.springframework.security.core.authority.SimpleGrantedAuthority
import org.springframework.security.core.context.SecurityContextHolder
import org.springframework.security.core.userdetails.UsernameNotFoundException
import org.springframework.test.context.ActiveProfiles
import org.springframework.test.context.junit4.SpringRunner
import javax.annotation.PostConstruct

@RunWith(SpringRunner::class)
@SpringBootTest
@FixMethodOrder(MethodSorters.JVM)
@ActiveProfiles("testdata")
abstract class BaseTest {

    @Autowired
    lateinit var userRepository: UserRepository

    fun useSecurityUser(user: User?, f: () -> Unit) {
        SecurityContextHolder.getContext().authentication =
                if (user != null)
                    UsernamePasswordAuthenticationToken(user.username,
                            "",
                            user.roles.map { SimpleGrantedAuthority(it.roleName) }.toMutableList())
                else null
        synchronized(SecurityContextHolder.getContext()) {
            f()
        }
    }

    fun useRoleUser(f: () -> Unit) = useSecurityUser(userRepository.findOne(1)!!, f)

    fun useRoleAdmin(f: () -> Unit) = useSecurityUser(userRepository.findByUsername("bankelini")!!, f)

    fun useRoleNone(f: () -> Unit) = useSecurityUser(null, f)

}