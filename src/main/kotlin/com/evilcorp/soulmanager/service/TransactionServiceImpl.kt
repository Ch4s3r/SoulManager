package com.evilcorp.soulmanager.service

import com.evilcorp.soulmanager.entity.Transaction
import com.evilcorp.soulmanager.entity.valueablegood.ValueAbleGood
import com.evilcorp.soulmanager.repository.TransactionRepository
import com.evilcorp.soulmanager.repository.ValueAbleGoodRepository
import mu.KotlinLogging
import org.springframework.stereotype.Service

@Service
class TransactionServiceImpl(val valueAbleGoodRepository: ValueAbleGoodRepository,
                             val transactionRepository: TransactionRepository) : TransactionService {

    private val log = KotlinLogging.logger {}

    override fun send(fromValueAbleGood: ValueAbleGood, toValueAbleGood: ValueAbleGood, amount: Long): Boolean {
        return if (withdraw(fromValueAbleGood, amount)) {
            deposit(toValueAbleGood, amount)
            val transaction = transactionRepository.save(Transaction(from = fromValueAbleGood, to = toValueAbleGood, amount = amount))
            log.info("Transaction from ${fromValueAbleGood.id} to ${toValueAbleGood.id} was successful: ID ${transaction.id}")
            true
        } else {
            log.info("Transaction from ${fromValueAbleGood.id} to ${toValueAbleGood.id} failed")
            false
        }
    }

    override fun withdraw(valueAbleGood: ValueAbleGood, amount: Long): Boolean {
        return if (valueAbleGood.value >= amount) {
            log.info("Withdrawing $amount gold from ${valueAbleGood.owner.fullname}'s ${valueAbleGood.name}")
            valueAbleGood.value -= amount
            valueAbleGoodRepository.save(valueAbleGood)
            true
        } else {
            log.info("Cannot withdraw $amount gold from ${valueAbleGood.owner.fullname}'s ${valueAbleGood.name}. Balance is too low.")
            false
        }
    }

    override fun deposit(valueAbleGood: ValueAbleGood, amount: Long): Boolean {
        log.info("Depositing $amount gold to ${valueAbleGood.owner.fullname}'s ${valueAbleGood.name}")
        valueAbleGood.value += amount
        valueAbleGoodRepository.save(valueAbleGood)
        return true
    }
}