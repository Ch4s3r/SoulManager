package com.evilcorp.soulmanager

import com.evilcorp.soulmanager.entity.Role
import com.evilcorp.soulmanager.repository.RoleRepository
import org.springframework.stereotype.Component
import javax.annotation.PostConstruct

@Component
class RoleInitializer(val roleRepository: RoleRepository) {
    @PostConstruct
    fun init() {
        if (roleRepository.count() == 0L)
            roleRepository.saveAll(listOf(Role(name = "ADMIN"), Role(name = "USER")))
    }
}