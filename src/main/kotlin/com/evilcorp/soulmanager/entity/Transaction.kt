package com.evilcorp.soulmanager.entity

import com.evilcorp.soulmanager.entity.valueablegood.ValueAbleGood
import java.time.Instant
import javax.persistence.*

@Entity
data class Transaction(
        @Id @GeneratedValue(strategy = GenerationType.IDENTITY) val id: Long = 0,
        @ManyToOne val from: ValueAbleGood? = null,
        @ManyToOne val to: ValueAbleGood? = null,
        val amount: Long = 0,
        val time: Instant = Instant.now())
