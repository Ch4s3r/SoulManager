package com.evilcorp.soulmanager.repository

import org.springframework.data.jpa.repository.JpaRepository

fun <T> JpaRepository<T, Long>.findOne(id: Long): T? {
    val o = findById(id)
    return if(o.isPresent) o.get() else null
}