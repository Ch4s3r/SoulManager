package com.evilcorp.soulmanager.repository

import com.evilcorp.soulmanager.entity.valueablegood.ValueAbleGood
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.stereotype.Repository

@Repository
interface ValueAbleGoodRepository : JpaRepository<ValueAbleGood, Long> {
    fun findByOwnerUsername(username: String): List<ValueAbleGood>
}