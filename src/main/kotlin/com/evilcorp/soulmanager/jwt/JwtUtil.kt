package com.evilcorp.soulmanager.jwt

import com.evilcorp.soulmanager.entity.Role
import com.evilcorp.soulmanager.entity.User
import com.evilcorp.soulmanager.repository.RoleRepository
import com.evilcorp.soulmanager.repository.UserRepository
import io.jsonwebtoken.Jwts
import io.jsonwebtoken.SignatureAlgorithm
import mu.KotlinLogging
import org.springframework.context.annotation.DependsOn
import org.springframework.stereotype.Component
import java.time.Instant
import java.util.*
import javax.annotation.PostConstruct
import javax.management.relation.RoleNotFoundException

@Component
@DependsOn("roleInitializer")
class JwtUtil(val userRepository: UserRepository, val roleRepository: RoleRepository) {

    private val log = KotlinLogging.logger {}

    //read value from external file for more security ;)
    //    @Value("\${jwt.secret}")
    val secret: String = "your-256-bit-secret"
    lateinit var roles: Map<String, Role>

    @PostConstruct
    fun init() {
        roles = roleRepository.findAll().map {
            it.roleName to it
        }.toMap()
    }

    fun parseToken(token: String): User {
        return try {
            val body = Jwts.parser().setSigningKey(secret).parseClaimsJws(token).body
            User(username = body["name"].toString(),
                    id = body.subject.toLong(),
                    roles = (body["roles"] as List<*>).map {
                        roles[it] ?: throw RoleNotFoundException()
                    }.toList())
        } catch (e: Exception) {
            throw Exception("Failed to parse json token: $token")
        }
    }

    fun generateToken(user: User): String {
        val claims = Jwts.claims().setSubject(user.id.toString()).setExpiration(Date.from(Instant.now().plusSeconds(24 * 60 * 60)))
        val userDB = userRepository.findByUsername(user.username)
                ?: throw Exception("username not found: ${user.username}")
        claims["name"] = userDB.username
        claims["roles"] = userDB.roles.map { it.roleName }
        return Jwts.builder().setHeaderParam("typ", "JWT").setClaims(claims).signWith(SignatureAlgorithm.HS512, secret).compact()
    }
}