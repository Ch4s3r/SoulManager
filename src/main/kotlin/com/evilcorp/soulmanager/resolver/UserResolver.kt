package com.evilcorp.soulmanager.resolver

import com.coxautodev.graphql.tools.GraphQLMutationResolver
import com.coxautodev.graphql.tools.GraphQLQueryResolver
import com.evilcorp.soulmanager.entity.User
import com.evilcorp.soulmanager.entity.valueablegood.ValueAbleGood
import com.evilcorp.soulmanager.repository.UserRepository
import com.evilcorp.soulmanager.repository.ValueAbleGoodRepository
import com.evilcorp.soulmanager.repository.findOne
import org.springframework.security.access.prepost.PreAuthorize
import org.springframework.security.core.userdetails.UsernameNotFoundException
import org.springframework.stereotype.Component

@Component
class UserResolver(val userRepository: UserRepository,
                   val valueAbleGoodRepository: ValueAbleGoodRepository) : GraphQLQueryResolver, GraphQLMutationResolver {

    @PreAuthorize("hasRole('USER')")
    fun listUsers(): List<User> {
        return userRepository.findAll()
    }

    @PreAuthorize("hasRole('ADMIN')")
    fun listUser(id: Long): User? {
        return userRepository.findOne(id)
    }

    @PreAuthorize("hasRole('USER')")
    fun listAccount(valueAbleGoodID: Long): ValueAbleGood {
        val user = userRepository.findByUsername(currentUsername())
                ?: throw UsernameNotFoundException(currentUsername())
        val valueAbleGood = valueAbleGoodRepository.findOne(valueAbleGoodID)
                ?: throw Exception("Account $valueAbleGoodID not found.")
        if (user.id == valueAbleGood.owner.id || isAdmin())
            return valueAbleGood
        else
            throw Exception("User '${user.username}' is not the owner of: $valueAbleGoodID")
    }

    @PreAuthorize("hasRole('USER')")
    fun listMyAccounts(): List<ValueAbleGood> {
        return valueAbleGoodRepository.findByOwnerUsername(currentUsername())
    }
}