package com.evilcorp.soulmanager.resolver

import com.coxautodev.graphql.tools.GraphQLMutationResolver
import com.coxautodev.graphql.tools.GraphQLQueryResolver
import com.evilcorp.soulmanager.repository.UserRepository
import com.evilcorp.soulmanager.repository.ValueAbleGoodRepository
import com.evilcorp.soulmanager.repository.findOne
import com.evilcorp.soulmanager.service.TransactionService
import org.springframework.security.access.prepost.PreAuthorize
import org.springframework.security.core.userdetails.UsernameNotFoundException
import org.springframework.stereotype.Component

@Component
class TransactionResolver(val transactionService: TransactionService,
                          val valueAbleGoodRepository: ValueAbleGoodRepository,
                          val userRepository: UserRepository) : GraphQLQueryResolver, GraphQLMutationResolver {

    @PreAuthorize("hasRole('USER')")
    fun withdraw(valueAbleGoodID: Long, amount: Long): Boolean {
        val user = userRepository.findByUsername(currentUsername())
                ?: throw UsernameNotFoundException("")
        val valueAbleGood = valueAbleGoodRepository.findOne(valueAbleGoodID)
                ?: throw Exception("Account $valueAbleGoodID not found")
        if (user.id == valueAbleGood.owner.id) {
            return transactionService.withdraw(valueAbleGood, amount)
        }
        return false
    }

    @PreAuthorize("hasRole('USER')")
    fun deposit(valueAbleGoodID: Long, amount: Long): Boolean {
        val user = userRepository.findByUsername(currentUsername())
                ?: throw UsernameNotFoundException("")
        val valueAbleGood = valueAbleGoodRepository.findOne(valueAbleGoodID)
                ?: throw Exception("Account $valueAbleGoodID not found")
        if (user.id == valueAbleGood.owner.id) {
            return transactionService.deposit(valueAbleGood, amount)
        }
        return false
    }

    @PreAuthorize("hasRole('USER')")
    fun send(fromValueAbleGoodID: Long, toValueAbleGoodID: Long, amount: Long): Boolean {
        val user = userRepository.findByUsername(currentUsername())
                ?: throw UsernameNotFoundException("")
        val fromValueAbleGood = valueAbleGoodRepository.findOne(fromValueAbleGoodID)
                ?: throw  Exception("Account $fromValueAbleGoodID not found")
        val toValueAbleGood = valueAbleGoodRepository.findOne(toValueAbleGoodID)
                ?: throw  Exception("Account $toValueAbleGoodID not found")
        if (user.id == fromValueAbleGood.owner.id && fromValueAbleGoodID != toValueAbleGoodID) {
            return transactionService.send(fromValueAbleGood, toValueAbleGood, amount)
        }
        return false
    }

}