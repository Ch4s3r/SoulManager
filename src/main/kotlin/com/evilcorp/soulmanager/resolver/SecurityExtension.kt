package com.evilcorp.soulmanager.resolver

import com.coxautodev.graphql.tools.GraphQLResolver
import org.springframework.security.core.authority.SimpleGrantedAuthority
import org.springframework.security.core.context.SecurityContextHolder

fun GraphQLResolver<Void>.currentUsername() = SecurityContextHolder.getContext().authentication.principal.toString()
fun GraphQLResolver<Void>.isAdmin() = SecurityContextHolder.getContext().authentication.authorities.contains(SimpleGrantedAuthority("ROLE_ADMIN"))