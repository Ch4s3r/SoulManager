# SoulManager
One manager to rule them all.

Requirements:<br>
https://gist.github.com/christophpickl/1f1847137d52d516c04d468d9aafad1c/revisions

# TODOs would be:

* Error messages for the user -> currently only 500 internal server error on fail
* concurrent testing/thread safety
* investigate concurrent failing tests
* more fine grained test cases
* GraphQL test cases for resolvers (integration tests)
* use interfaces for all services in general?
* query.graphqls:
    * split up per use case such as 'user', 'transaction',... into own files
* use jwt refresh token (optional)
* transactions:
    * list own transactions
    * admin can list all transactions
* create/delete accounts
* create more exception classes instead of using Exception always
* optimize tests, reduce duplicate code (e.g. parameterized tests)
* replace magic values with constants
* satisfy customer ;)
